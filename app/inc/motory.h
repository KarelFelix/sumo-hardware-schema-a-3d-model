#ifndef DELAY_H
#define DELAY_H

#include "stm8s.h"

void motory_straight(void);
void motory_gay(void);
void motory_doleva(void);
void motory_doprava(void);
void motory_stop(void);

#endif