#include "ultrasonic_sensor.h"
#include "int_to_str.h"
#include "uart.h"
#include "delay.h"
#include "stm8s_it.h"

void tim3_init(void)
{
    TIM3_TimeBaseInit(TIM3_PRESCALER_2, 63999);
    TIM3_Cmd(ENABLE);
}

void tim3_reset(void)
{
    TIM3_SetCounter(0);
}

float tim3_get_distance(uint16_t tim3_value)
{
    uint16_t micros = tim3_value / 8;
    // send_str(int_to_str(micros));
    // send_str("\n\r");

    return ((micros * 0.034) / 2);
}

void first_echo(int time_for_trig_1, int time_for_trig_2)
{
    GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
    delay_ms(time_for_trig_1);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    delay_ms(time_for_trig_2);
    // enableInterrupts();
}