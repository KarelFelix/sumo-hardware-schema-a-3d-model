#include "delay.h"
#include "motory.h"
#include "stm8s_it.h"

void motory_straight(void)
{
    GPIO_WriteHigh(GPIOE, GPIO_PIN_1); // levý motor
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);

    GPIO_WriteHigh(GPIOC, GPIO_PIN_7); // pravý motor
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
}

void motory_gay(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_2);

    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
}

void motory_doleva(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteHigh(GPIOE, GPIO_PIN_2);

    GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
}

void motory_doprava(void)
{
    GPIO_WriteHigh(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);

    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
    GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
}

void motory_stop(void)
{
    GPIO_WriteLow(GPIOE, GPIO_PIN_1);
    GPIO_WriteLow(GPIOE, GPIO_PIN_2);

    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
}