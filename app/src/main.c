#include "stm8s.h"
#include "uart.h"
#include "ultrasonic_sensor.h"
#include "int_to_str.h"
#include "delay.h"
#include "motory.h"

// globální proměnné
uint16_t tim3_value;
float vzdalenost;
int rise_fall = 1;

// interupt infra Red
INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
{
    // interupt po detekování bílého okraje
    disableInterrupts();
    GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    motory_gay();
    delay_ms(700);
    motory_stop();
    delay_ms(100);
    motory_doprava();
    delay_ms(700);
    motory_stop();
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
    enableInterrupts();
}

// interupt ultrazvuk
INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
{
    if (rise_fall == 1)
    {
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
        tim3_reset();
        TIM3_ClearFlag(TIM3_FLAG_UPDATE);
        rise_fall = 0;
    }
    else if (rise_fall == 0)
    {
        if (TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) == 0)
        {
            tim3_value = TIM3_GetCounter();
        }
        else if (TIM3_GetFlagStatus(TIM3_FLAG_UPDATE) == 1)
        {
            tim3_value = 63999;
        }
        GPIO_WriteLow(GPIOC, GPIO_PIN_5);
        rise_fall = 1;
        // disableInterrupts();
        // send_str(int_to_str(tim3_value));
        // send_str("\n\r");
    }
}

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);           // FREQ MCU 16MHz
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW); // Ledka na testování

    // ultrasonic sensor init ports
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW); // trig
    GPIO_Init(GPIOD, GPIO_PIN_3, GPIO_MODE_IN_FL_IT);        // echo

    // Infra Red sensor init ports
    GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_IN_FL_IT); // left
    GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_IN_FL_IT); // right

    // motory init ports
    GPIO_Init(GPIOE, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW); // motor B_1A
    GPIO_Init(GPIOE, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW); // motor B_1B
    GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW); // motor A_1A
    GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW); // motor A_1B

    // ultrasonic sensor interrupts
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_RISE_FALL); // interrupts settup for port D
    ITC_SetSoftwarePriority(ITC_IRQ_PORTD, ITC_PRIORITYLEVEL_1);

    // Infra Red sensor interrupts
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_FALL_ONLY);
    ITC_SetSoftwarePriority(ITC_IRQ_PORTB, ITC_PRIORITYLEVEL_2);

    enableInterrupts();

    uart1_init();

    tim4_init();

    tim3_init();

    delay_ms(5000);
    motory_straight();
    delay_ms(100);
    motory_stop();
    // delay_ms(300);
    while (1)
    {
        // delay_ms(100);
        // GPIO_WriteHigh(GPIOC,GPIO_PIN_4);
        // delay_ms(10);
        // GPIO_WriteLow(GPIOC,GPIO_PIN_4);
        // delay_ms(500);
        first_echo(10, 500);

        vzdalenost = tim3_get_distance(tim3_value);
        uint16_t hodnota_vzdalenosti = vzdalenost;

        // send_str(int_to_str(hodnota_vzdalenosti));
        // send_str("\n\r");

        if (hodnota_vzdalenosti < 60)
        {
            motory_straight();
        }
        else if (hodnota_vzdalenosti > 60)
        {
            // motory_gay();
            // delay_ms(500);
            motory_doleva();
            delay_ms(100);
            motory_stop();
            // delay_ms(500);
        }
        // else if (hodnota_vzdalenosti >170)
        //{
        // motory_doleva();
        // delay_ms(1000);
        // motory_stop();
        // delay_ms(5000);

        // motory_doprava();
        // delay_ms(1000);
        // motory_stop();
        // delay_ms(5000);
        // }
    }
}