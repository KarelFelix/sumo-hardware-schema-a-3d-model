<h1 align="center"> MINI Sumo Robot </h1>
<p align="center">Závěrečný projekt MIT: Lukáš Konečný a Karel Felix Šťastný.</p>
<p align="center">3.B, 2021/22</p>

---
## ÚVOD

<p>Mini sumo je jednou z kategorií zápasení mezi roboty. Cílem zápasenu je vystrčit svého oponenta z arény,
 robot ale musí splňovat podmínky dané kategorie. V této kategorii to je velikost 10x10 cm (jakákoliv výška) a váha nesmí přesahovat 500 g.
</p>

---
## Vývojový diagram


<p>Základní chování robota</p>

---
```mermaid
graph LR
START --> Základní_pohyb
Základní_pohyb --> B{Vidím_Soupeře} --> |NE| Pootočení_doleva
B --> |ANO| Jeď_dopředu--> KONEC

```

---
    
## Blokové schéma 
Blokové schéma se skládá z: Dvou infračervených senzorů, jednoho ultrazvukového senzoru, STM8, napájení, H-můstku a dvou motorů

```mermaid
flowchart LR
Napájení --> STM8
Napájení --> IR_Senzor1
Napájení --> IR_Senzor2
Napájení --> Ultrasonic_Senzor
Napájení --> H_Můstek
IR_Senzor1 <--> STM8
IR_Senzor2 <--> STM8
Ultrasonic_Senzor <--> STM8
STM8 --> H_Můstek
H_Můstek --> MOTORY
```
---

## Schéma
Základní schéma zapojení příslušných pinů
<p align="center"><img src="schema_sumo.PNG"><p>

## Hardwarová část
### - Seznam použitých součástek
- Ultrazvukový senzor HC-SR04
- 2x Infračervený senzor FC-51
- H-můstek modul L9110S
- 2x TT motor s převodovkou
- 2x 9V baterie
- Stabilizátor napětí L7805
- STM8 Nucleo
- Vypínač
- Dráty, Šroubky a matičky

Návrh komponent: Tělo bylo navrhnuto v programu Blender a bylo vytištěno na 3D tiskárně. Hýbe se pomocí dvou motorů, připojené na kole ve spodní části. Pro orientaci má ve předu uprostřed ultrazvukový senzor a ve předu po stranách 2 infračervené. Driver, napájení a kabeláž je umístěna uvnitř v jádru. Jako střecha nad hlavou mu poslouží zasouvací STM8.
<p align="center"><img src="render.png"><p>
<p align="center"><img src="fotka.PNG"><p>

---

## Propojení komponent, programová část
Robot je ovládaný přes STM8, přes několik vstupů a výstupů je propojený k modulům. Robot vnímá své okolí pomocí ultrazvukového senzoru, který pozná vzdálenost oběktu před ním. Celou dobu přitom infračervené senzory kontrolují přítomnost bílé barvy pod ním.

### - Programování
Program je rozdělen do knihoven a doplněn občasnými komentáři
<p align="center"><img src="directory.png"><p>

Několik ukázek:

```c
void motory_straight(void)
{
    GPIO_WriteHigh(GPIOE,GPIO_PIN_1); //levý motor
    GPIO_WriteLow(GPIOE,GPIO_PIN_2);

    GPIO_WriteHigh(GPIOC,GPIO_PIN_7); //pravý motor
    GPIO_WriteLow(GPIOC,GPIO_PIN_6);
}
```
- Funkce v souboru motory.c pro jízdu vpřed

```c
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    GPIO_Init(GPIOC,GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW); // Ledka na testování

    // ultrasonic sensor init ports
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW); // trig
    GPIO_Init(GPIOD, GPIO_PIN_3, GPIO_MODE_IN_FL_IT); // echo

    // Infra Red sensor init ports
    GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_IN_FL_IT); //left
    GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_IN_FL_IT); //right

    // motory init ports
    GPIO_Init(GPIOE, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW); //motor B_1A
    GPIO_Init(GPIOE, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW); //motor B_1B
    GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW); //motor A_1A
    GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW); //motor A_1B

    // ultrasonic sensor interrupts 
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD, EXTI_SENSITIVITY_RISE_FALL); // interrupts settup for port D
    ITC_SetSoftwarePriority(ITC_IRQ_PORTD, ITC_PRIORITYLEVEL_1);

    // Infra Red sensor interrupts
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOB, EXTI_SENSITIVITY_FALL_ONLY);
    ITC_SetSoftwarePriority(ITC_IRQ_PORTB, ITC_PRIORITYLEVEL_2);

    enableInterrupts();
    
    uart1_init();
    
    tim4_init();
    
    tim3_init();
```
- Inicializace potřebných timerů, interuptů, pinů a STM8 (včetně testovacích nástrojů, př: UART, PC5)

```c
float tim3_get_distance(uint16_t tim3_value)
{
    uint16_t micros = tim3_value/8;
    //send_str(int_to_str(micros));
    //send_str("\n\r");

    return((micros*0.034)/2);
}
```
- Výpočet vzdálenosti podle timeru3

```c
//interupt infra Red
INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
{
    //interupt po detekování bílého okraje
    disableInterrupts();
    GPIO_WriteHigh(GPIOC,GPIO_PIN_5);
    motory_gay();
    delay_ms(700);
    motory_stop();
    delay_ms(100);
    motory_doprava();
    delay_ms(700);
    motory_stop();
    GPIO_WriteLow(GPIOC,GPIO_PIN_5);
    enableInterrupts();
}
```

- Interrupt handler pro infračervené senzory.
---
## Závěr
Naučili jsme se toho dost, seznámili s novým hardwarem a světem robotiky.
Při tomto projetku jsme se ale dopustili pár chyb, jmenovitě pak špatné volby napájení (za následeky: zpomalení testování, při malém vybití blbnutí H-můstku) a příliž velký 
důraz na splnění stardantních rozměrů. Dále jsme narazili na pár problému, se kterými jsme se dokázali vypořádat. Kdybychom měli více času (např: kdyby jeden z nás na této práci pokračoval na dlouhodobou maturitu) ještě bychom ho vylepšili, třeba by došla řada na náš vysněný plamenomet.
    
---
    
## Rozdělení práce
Karel Felix Šťastný: 
- Návrh mechanické části       
- Zajištění součástek 
- Modelování a 3D tisk
- Zapojení
- Sestavení
- Dokumentaci (Schéma, Seznam součástek)
    

Lukáš Konečný:
- Zapojení
- Sestavení
- Návrh napájení
- Návrh programu a vyvojového diagramu
- Programování
- Finální program
- Dokumentace (Zbytek dokumentace)
    
 Všechny části projektu jsme spolu konzultovali, takže rozdělení není plně jednoznačné.

 Projekt měl prodloužený termín pro odevzdámí do 19.06.2022!
